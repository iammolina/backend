<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/libros', 'LibrosController@index');


Route::group(['prefix' => 'api'], function () use ($router) {
    $router->group(['prefix' => 'entidad' , 'middleware' => 'jwt.verify'], function () use ($router) {
        $router->get('/{entidad}', 'GenericController@index');
        $router->get('/{entidad}/list', 'GenericController@list');
        $router->get('/{entidad}/form', 'GenericController@form');
        $router->post('/{entidad}', 'GenericController@store');
        $router->put('/{entidad}/{id}', 'GenericController@update');
        $router->delete('/{entidad}/{id}', 'GenericController@destroy');
    });
    $router->group(['prefix' => 'factura' , 'middleware' => 'jwt.verify'], function () use ($router) {
        $router->get('/form', 'FacturaController@index');
        $router->post('/form', 'FacturaController@store');
        $router->get('/form/{id}', 'FacturaController@getId');
    });
    $router->group(['prefix' => 'boleta' , 'middleware' => 'jwt.verify'], function () use ($router) {
        $router->get('/form', 'BoletaController@index');
        $router->post('/form', 'BoletaController@store');
        $router->get('/form/{id}', 'BoletaController@getId');
    });
    $router->group(['prefix' => 'arriendo' , 'middleware' => 'jwt.verify'], function () use ($router) {
        $router->get('/form', 'ArriendoContoller@index');
        $router->post('/form', 'ArriendoContoller@store');
        $router->get('/form/{id}', 'ArriendoContoller@getId');
        $router->get('/form/pay/{id}', 'ArriendoContoller@formPay');
        $router->post('/form/pay/{id}', 'ArriendoContoller@pay');
    });

    
});