<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Folio {{$factura->folio}}</title>
    </head>
    <body>
       <h1>Folio:<small>{{$factura->folio}}</small></h1>
       <h4>Distribuidor:<small>{{$factura->distribuidores['nombres']}}</small></h4>
       <h4>Medio de pago:<small>{{$factura->metodoDePago['nombre']}}</small></h4>
       <h4>Fecha de compra: <bold>{{$factura->fecha_de_compra}}</bold></h4>

       <table>
        <tr>
          <th>Firstname</th>
          <th>Lastname</th>
        </tr>
        <tr>
          <td>Peter</td>
          <td>Griffin</td>
        </tr>
        <tr>
          <td>Lois</td>
          <td>Griffin</td>
        </tr>
      </table>
    </body>
</html>
