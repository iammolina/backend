<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAtributteDetalleArriendo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalle_arriendo', function (Blueprint $table) {
            $table->decimal('valor')->nullable();
            $table->integer('boleta')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_arriendo', function (Blueprint $table) {
            $table->dropColumn('valor');
            $table->dropColumn('boleta');
        });
    }
}
