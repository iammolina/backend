<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibrosAutores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros_autores', function($table) {
            $table->increments('id', true);
            $table->unsignedBigInteger('libros_id');
            $table->foreign('libros_id')->references('id')->on('libros');
            $table->unsignedBigInteger('autores_id');
            $table->foreign('autores_id')->references('id')->on('autores');
            $table->timestamps();
            $table->softDeletes();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros_autores');
    }
}
