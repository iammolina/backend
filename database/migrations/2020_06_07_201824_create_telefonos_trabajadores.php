<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelefonosTrabajadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telefonos_trabajadores', function (Blueprint $table) {
            $table->increments('id', true);
            $table->integer('telefono')->default(0);
            $table->unsignedBigInteger('trabajadores_id');
            $table->foreign('trabajadores_id')->references('id')->on('trabajadores');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telefonos_trabajadores');
    }
}
