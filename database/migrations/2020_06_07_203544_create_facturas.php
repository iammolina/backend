<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->id();
            $table->string('folio');
            $table->decimal('precio_neto');
            $table->decimal('precio_con_iva');
            $table->decimal('costo_iva');
            $table->date('fecha_de_compra');
            $table->unsignedBigInteger('distribuidores_id');
            $table->foreign('distribuidores_id')->references('id')->on('distribuidores');
            $table->unsignedBigInteger('metodo_de_pago_id');
            $table->foreign('metodo_de_pago_id')->references('id')->on('metodo_de_pago');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
