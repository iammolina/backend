<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIsbn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isbn', function (Blueprint $table) {
            $table->id();
            $table->string('codigo');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('libros', function (Blueprint $table) {
            $table->unsignedBigInteger('isbn_id')->after('publicacion');
            $table->foreign('isbn_id')->references('id')->on('isbn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('libros', function (Blueprint $table) {
            $table->dropForeign(['isbn_id']);
            $table->dropColumn('isbn_id');
        });
        Schema::dropIfExists('isbn');
    }
}
