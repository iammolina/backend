<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArriendos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('arriendos', function (Blueprint $table) {
            $table->id();
            $table->string('costo_total');
            $table->date('fecha_de_arriendo');
            $table->date('fecha_de_devolucion_estimada');
            $table->date('fecha_de_entrega_real');
            $table->date('dias_de_retraso');
            $table->decimal('multa');
            $table->decimal('costo_de_arriendo');
            $table->unsignedBigInteger('clientes_id');
            $table->foreign('clientes_id')->references('id')->on('clientes');
            $table->unsignedBigInteger('trabajadores_id');
            $table->foreign('trabajadores_id')->references('id')->on('trabajadores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arriendos');
    }
}
