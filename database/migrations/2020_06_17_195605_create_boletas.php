<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       //Folio, precio neto, precio con IVA, costo IVA, fecha de venta, hora de venta, cliente involucrado, trabajador involucrado, método de pago.
        Schema::create('boletas', function (Blueprint $table) {
            $table->id();
            $table->string('folio');
            $table->decimal('precio_neto');
            $table->decimal('precio_con_iva');
            $table->decimal('costo_iva');
            $table->date('fecha_de_venta');
            $table->unsignedBigInteger('clientes_id');
            $table->foreign('clientes_id')->references('id')->on('clientes');
            $table->unsignedBigInteger('trabajadores_id');
            $table->foreign('trabajadores_id')->references('id')->on('trabajadores');
            $table->unsignedBigInteger('metodo_de_pago_id');
            $table->foreign('metodo_de_pago_id')->references('id')->on('metodo_de_pago');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletas');
    }
}
