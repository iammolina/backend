<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleArriendo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_arriendo', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('arriendos_id');
            $table->foreign('arriendos_id')->references('id')->on('arriendos');
            $table->unsignedBigInteger('libros_id');
            $table->foreign('libros_id')->references('id')->on('libros');
            $table->integer('cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_arriendo');
    }
}
