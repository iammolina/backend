<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDireccionesTrabajadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones_trabajadores', function (Blueprint $table) {
            $table->increments('id', true);
            $table->integer('direccion')->default(0);
            $table->unsignedBigInteger('trabajadores_id');
            $table->foreign('trabajadores_id')->references('id')->on('trabajadores');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones_trabajadores');
    }
}
