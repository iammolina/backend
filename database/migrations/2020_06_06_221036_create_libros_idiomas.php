<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibrosIdiomas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros_idiomas', function($table) {
            $table->increments('id', true);
            $table->unsignedBigInteger('libros_id');
            $table->foreign('libros_id')->references('id')->on('libros');
            $table->unsignedBigInteger('idiomas_id');
            $table->foreign('idiomas_id')->references('id')->on('idiomas');
            $table->timestamps();
            $table->softDeletes();
        });
    
      
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libros_idiomas');
    }
}
