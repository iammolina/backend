<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleCompras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_compras', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('compras_id');
            $table->foreign('compras_id')->references('id')->on('compras');
            $table->unsignedBigInteger('libros_id');
            $table->foreign('libros_id')->references('id')->on('libros');
            $table->unsignedBigInteger('distribuidores_id');
            $table->foreign('distribuidores_id')->references('id')->on('distribuidores');
            $table->integer('cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_compras');
    }
}
