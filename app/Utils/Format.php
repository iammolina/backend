<?php

namespace App\Utils;

class Format
{
    public static function entidadClass($entidad)
    {
        return ucfirst(strtolower($entidad));
    }

    public static function clean($string)
    {
        $string = preg_replace('/\(\d\)/', '', $string);
        // $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return strtolower($string);
    }

    public static function json_change_key($arr, $oldkey, $newkey)
    {
        $json = str_replace('"' . $oldkey . '":', '"' . $newkey . '":', json_encode($arr));
        return json_decode($json, true);
    }

    public static function formatRespuestasRsh($respuestas)
    {
        if (is_array($respuestas) || is_object($respuestas)) {
            foreach ($respuestas as $key => $respuesta) {
                $formatRespuesta = preg_replace('/\d+\./i', '', $respuesta);
                $formatRespuesta = preg_replace('/^\s/i', '', $formatRespuesta);
                $respuestas[$key] = $formatRespuesta;
            }
        }
        return $respuestas;
    }
    public static function formatRespuestasRshAutoReporte($respuestas)
    {
        if (is_array($respuestas) || is_object($respuestas)) {
            foreach ($respuestas as $key => $respuesta) {
                unset($respuestas[$key]['respuesta-municipal']);
                $respuestas[$key]['label'] = preg_replace('/\d+\./i', '', $respuesta['label']);
                $respuestas[$key]['label'] = preg_replace('/^\s/i', '', $respuesta['label']);
            }
        }
        return $respuestas;
    }

    public static function replaceKeyRsh($data, $label = 'objeto_nomb', $value = 'objeto_codigo')
    {
        $data = Format::json_change_key($data, $label, 'label');
        $data = Format::json_change_key($data, $value, 'value');
        return $data;
    }

    public static function formatkey($data)
    {
        $response = [];
        foreach ($data as $key => $value) {
            $response['label'] = $value;
            $response['value'] = $key;
        }
        return $response;
    }

    public static function nestedLowercase($value)
    {
        if (is_array($value)) {
            return array_map('self::nestedLowercase', $value);
        }
        return ucfirst(mb_strtolower($value, 'UTF-8'));
    }

    public static function recursive_unset(&$array, $keys = [])
    {
        foreach ($keys as $key) {
            unset($array[$key]);
        }
    }

    public static function deleteAcentos($string)
    {
        return str_replace(array('Á', 'á', 'É', 'é', 'Í', 'í', 'Ó', 'ó', 'Ú', 'ú'), array('A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u'), $string);
    }

    public static function formatDate($day, $month, $year)
    {
        return str_pad($day, 2, "0", STR_PAD_LEFT) . "-" . str_pad($month, 2, "0", STR_PAD_LEFT) . "-" . $year;
    }
}
