<?php

namespace App\Models;


use Illuminate\Database\Eloquent\{Model};

class LibrosCategorias extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'libros_categorias';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

   
}
