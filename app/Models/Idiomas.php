<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model,SoftDeletes};

class Idiomas extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'idiomas';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'nombres' => 'required',
        ];
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function table()
    {
        return strtolower($this->table);
    }

    public static function data($request, &$roleData)
    {
        $roleData->nombres = $request->has('nombres') ? $request->nombres : 0;
    }

    public static function mapColumns()
    {
        return ['id as value', 'nombres as label'];
    }
}
