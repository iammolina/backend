<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model,SoftDeletes};

class Isbn extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'isbn';

    public static function rules()
    {
        return [
            'codigo' => 'required',
        ];
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function table()
    {
        return strtolower($this->table);
    }

    public static function data($request, &$roleData)
    {
        $roleData->codigo = $request->has('codigo') ? $request->codigo : 0;
    }

    public static function mapColumns()
    {
        return ['id as value', 'codigo as label'];
    }
}
