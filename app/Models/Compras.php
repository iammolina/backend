<?php

namespace App\Models;
use Illuminate\Database\Eloquent\{Model ,SoftDeletes};


class Compras extends Model
{
    //use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'compras';


      protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'facturas_id' => 'required',
            'fecha_creacion' => 'required',
        ];
    }

    protected $fillable = ['facturas_id','fecha_creacion'];


    public function table()
    {
        return strtolower($this->table);
    }


    public static function data($request, &$roleData)
    {
        $roleData->facturas_id = $request->has('facturas_id') ? $request->facturas_id : 0;
        $roleData->fecha_creacion = $request->has('fecha_creacion') ? $request->fecha_creacion : 0;
    }
   

    public function Facturas()
    {
        return $this->belongsTo('App\Models\Facturas','facturas_id','id');
    }

    public function DetalleCompras()
    {
        return $this->hasMany('App\Models\DetalleCompras');
    }

    public function withTable()
    {
        return ['Facturas'];
    }
}
