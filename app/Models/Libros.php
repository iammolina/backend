<?php

namespace App\Models;
use Illuminate\Database\Eloquent\{Model ,SoftDeletes };


class Libros extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'libros';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'titulo' => 'required',
            'numero_serie' => 'unique:libros,numero_serie'
        ];
    }

    protected $fillable = ['titulo', 'numero_serie', 'numero_paginas', 'precio', 'publicacion', 'isbn_id', 'editorial_id', 'libros_estados_id'];


    public function table()
    {
        return strtolower($this->table);
    }


    public static function data($request, &$roleData)
    {
        $roleData->titulo = $request->has('titulo') ? $request->titulo : 0;
        $roleData->numero_serie = $request->has('numero_serie') ? $request->numero_serie : Libros::getNumeroAtencion();
        $roleData->numero_de_paginas = $request->has('numero_de_paginas') ? $request->numero_de_paginas : 0;
        $roleData->precio = $request->has('precio') ? $request->precio : 0;
        $roleData->ano = $request->has('ano') ? $request->ano : 0;
        $roleData->publicacion = $request->has('publicacion') ? $request->publicacion : 0;
        $roleData->isbn_id = $request->has('isbn_id') ? $request->isbn_id : 0;
        $roleData->editorial_id = $request->has('editorial_id') ? $request->editorial_id : 0;
        $roleData->libros_estados_id = $request->has('libros_estados_id') ? $request->libros_estados_id : 0;
    }


    public function LibrosEstados()
    {
        return $this->belongsTo('App\Models\LibrosEstados','libros_estados_id','id');
    }

    public function Editorial()
    {
        return $this->belongsTo('App\Models\Editorial','editorial_id','id');
    }

    public function Isbn()
    {
        return $this->belongsTo('App\Models\Isbn','isbn_id','id');
    }

    public function categorias()
    {
        return $this->belongsToMany('App\Models\Categorias', 'libros_categorias', 'libros_id', 'categorias_id');
    }
    public function autores()
    {
        return $this->belongsToMany('App\Models\Autores', 'libros_autores', 'libros_id', 'autores_id');
    }
    public function idiomas()
    {
        return $this->belongsToMany('App\Models\Idiomas', 'libros_idiomas', 'libros_id', 'idiomas_id');
    }

    public function withTable()
    {
        return ['Isbn','LibrosEstados','Editorial'];
    }

    // public function withTableMany()
    // {
    //     return ['autores'];
    // }

    public function isNotRequired()
    {
        return ['numero_serie'];
    }

    public function isDisabled()
    {
        return ['numero_serie'];
    }

    public static function getNumeroAtencion()
    {
        $libros = Libros::latest()->withTrashed()->first(['numero_serie']);
        if ($libros) {
            return ++$libros->numero_serie;
        }
        return 'A0000001';
    }
}
