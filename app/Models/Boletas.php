<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Boletas extends Model
{
    //use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'boletas';


      protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'folio' => 'required|unique:facturas,folio',
            'precio_neto' => 'required',
            'precio_con_iva' => 'required',
            'costo_iva' => 'required',
            'fecha_de_venta' => 'required',
            'clientes_id' => 'required',
            'trabajadores_id' => 'required',
            'metodo_de_pago_id' => 'required',
        ];
    }

    protected $fillable = ['folio', 'precio_neto', 'precio_con_iva', 'costo_iva', 'fecha_de_venta', 'clientes_id', 'trabajadores_id', 'metodo_de_pago_id'];


    public function table()
    {
        return strtolower($this->table);
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function Ventas()
    {
        return $this->belongsTo('App\Models\Ventas','id','boletas_id');
    }

    public function metodoDePago()
    {
        return $this->belongsTo('App\Models\MetodoDePago','metodo_de_pago_id','id');
    }
    public function Clientes()
    {
        return $this->belongsTo('App\Models\Clientes','clientes_id','id');
    }
    public function Trabajadores()
    {
        return $this->belongsTo('App\Models\Trabajadores','trabajadores_id','id');
    }

    public function withTable()
    {
        return ['metodoDePago','Clientes','Trabajadores'];
    }

    public static function mapColumns()
    {
        return ['id as value', 'folio as label'];
    }
}
