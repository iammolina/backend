<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model,SoftDeletes};

class Autores extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'autores';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];


    public static function rules()
    {
        return [
            'nombre' => 'required|max:10',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
        ];
    }
    

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function table()
    {
        return strtolower($this->table);
    }

    public static function data($request, &$roleData)
    {
        $roleData->nombre = $request->has('nombre') ? $request->nombre : 0;
        $roleData->apellido_paterno = $request->has('apellido_paterno') ? $request->apellido_paterno : 0;
        $roleData->apellido_materno = $request->has('apellido_materno') ? $request->apellido_materno : 0;
    }

    public static function mapColumns()
    {
        return ['id as value', 'nombre as label'];
    }

}
