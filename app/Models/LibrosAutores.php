<?php

namespace App\Models;


use Illuminate\Database\Eloquent\{Model};

class LibrosAutores extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'libros_autores';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

   
}
