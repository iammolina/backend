<?php

namespace App\Models;
use Illuminate\Database\Eloquent\{Model ,SoftDeletes};


class DetalleCompras extends Model
{
    //use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'detalle_compras';


      protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'compras_id' => 'required',
            'libros_id' => 'required',
            'distribuidores_id' => 'required',
            'cantidad' => 'required',
            'valor' => 'required',
        ];
    }

    protected $fillable = ['compras_id','libros_id' , 'distribuidores_id','cantidad','valor'];

    public function Compras()
    {
        return $this->belongsTo('App\Models\Compras','id','facturas_id');
    }

    public function Libros()
    {
        return $this->belongsTo('App\Models\Libros','libros_id','id');
    }

}
