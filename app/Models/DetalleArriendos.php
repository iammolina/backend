<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleArriendos extends Model
{
   
   protected $table = 'detalle_arriendo';


     protected $hidden = [
       'updated_at',
       'deleted_at',
   ];

   public static function rules()
   {
       return [
           'arriendo_id' => 'required',
           'libros_id' => 'required',
           'cantidad' => 'required',
           'valor' => 'required',
       ];
   }

   protected $fillable = ['arriendo_id','libros_id' ,'cantidad','valor'];

   public function Libros()
   {
       return $this->belongsTo('App\Models\Libros','libros_id','id');
   }
}
