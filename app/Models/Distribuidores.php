<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model,SoftDeletes};

class Distribuidores extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'distribuidores';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];


    public static function rules()
    {
        return [
            'rut' => 'required|max:10',
            'nombres' => 'required|max:10',
            'direccion' => 'required',
            'telefono' => 'required',
            'ano_distribuicion' => 'required',
        ];
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function table()
    {
        return strtolower($this->table);
    }

    public function isValidate()
    {
        return ['rut' => 'RutValid'];
    }

    public static function data($request, &$roleData)
    {
        $roleData->rut = $request->has('rut') ? $request->rut : 0;
        $roleData->nombres = $request->has('nombres') ? $request->nombres : "";
        $roleData->direccion = $request->has('direccion') ? $request->direccion : "";
        $roleData->telefono = $request->has('telefono') ? $request->telefono : "";
        $roleData->ano_distribuicion = $request->has('ano_distribuicion') ? $request->ano_distribuicion : 2020;
    }

    public static function mapColumns()
    {
        return ['id as value', 'nombres as label'];
    }

}
