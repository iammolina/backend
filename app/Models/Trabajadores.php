<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trabajadores extends Model
{
    protected $table = 'trabajadores';


    protected $hidden = [
      'updated_at',
      'deleted_at',
  ];

  public static function rules()
  {
      return [
          'rut' => 'required',
          'nombres' => 'required',
          'apellido_paterno' => 'required',
          'apellido_materno' => 'required',
          'fecha_inicio_contrato' => 'required',
      ];
  }

  protected $fillable = ['rut','nombres','apellido_paterno','apellido_materno','fecha_inicio_contrato'];


  public function table()
  {
      return strtolower($this->table);
  }

  public static function formWhere()
  {
      return [
          'where' => false
      ];
  }

  public static function data($request, &$roleData)
  {
      $roleData->rut = $request->has('rut') ? $request->rut : 0;
      $roleData->nombres = $request->has('nombres') ? $request->nombres : 0;
      $roleData->apellido_paterno = $request->has('apellido_paterno') ? $request->apellido_paterno : 0;
      $roleData->apellido_materno = $request->has('apellido_materno') ? $request->apellido_materno : 0;
      $roleData->fecha_inicio_contrato = $request->has('fecha_inicio_contrato') ? $request->fecha_inicio_contrato : 0;
  }

  public static function mapColumns()
  {
      return ['id as value', 'nombres as label'];
  }
}
