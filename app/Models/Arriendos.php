<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arriendos extends Model
{
     //use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'arriendos';


      protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [

            'fecha_de_arriendo' => 'required',
            'fecha_de_devolucion_estimada' => 'required',
            'costo_de_arriendo' => 'required',
            'clientes_id' => 'required',
            'trabajadores_id' => 'required',
        ];
    }

    protected $fillable = ['costo_total', 'fecha_de_arriendo', 'fecha_de_devolucion_estimada', 'fecha_de_entrega_real', 'dias_de_retraso', 'multa','clientes_id', 'trabajadores_id','boleta'];


    public function table()
    {
        return strtolower($this->table);
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function WhereNotIn(){
       return ['ID', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT', 'LOGIN_AT'];
    }

    public function DetalleArriendos()
    {
        return $this->hasMany('App\Models\DetalleArriendos');
    }
   
    public function Clientes()
    {
        return $this->belongsTo('App\Models\Clientes','clientes_id','id');
    }
    public function Trabajadores()
    {
        return $this->belongsTo('App\Models\Trabajadores','trabajadores_id','id');
    }

    public function withTable()
    {
        return ['Clientes','Trabajadores'];
    }

    public static function mapColumns()
    {
        return ['id as value', 'folio as label'];
    }
}
