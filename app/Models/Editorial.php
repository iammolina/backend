<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model,SoftDeletes};

class Editorial extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'editorial';

    public static function rules()
    {
        return [
            'nombre' => 'required',
        ];
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function table()
    {
        return strtolower($this->table);
    }

    public static function data($request, &$roleData)
    {
        $roleData->nombre = $request->has('nombre') ? $request->nombre : 0;
    }

    public static function mapColumns()
    {
        return ['id as value', 'nombre as label'];
    }
}
