<?php

namespace App\Models;
use Illuminate\Database\Eloquent\{Model ,SoftDeletes };


class Facturas extends Model
{
    //use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facturas';


      protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'folio' => 'required|unique:facturas,folio',
            'precio_neto' => 'required',
            'precio_con_iva' => 'required',
            'costo_iva' => 'required',
            'fecha_de_compra' => 'required',
            'distribuidores_id' => 'required',
            'metodo_de_pago_id' => 'required',
        ];
    }

    protected $fillable = ['folio', 'precio_neto', 'precio_con_iva', 'costo_iva', 'fecha_de_compra', 'distribuidores_id', 'metodo_de_pago_id'];


    public function table()
    {
        return strtolower($this->table);
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function Compras()
    {
        return $this->belongsTo('App\Models\Compras','id','facturas_id');
    }

    public function distribuidores()
    {
        return $this->belongsTo('App\Models\Distribuidores','distribuidores_id','id');
    }

    public function metodoDePago()
    {
        return $this->belongsTo('App\Models\MetodoDePago','metodo_de_pago_id','id');
    }

    public function withTable()
    {
        return ['distribuidores','metodoDePago'];
    }

   

    public static function mapColumns()
    {
        return ['id as value', 'folio as label'];
    }
}
