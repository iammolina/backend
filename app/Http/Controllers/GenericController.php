<?php

namespace App\Http\Controllers;

use App\Exports\GenericExport;
use App\Models\UserTabCols;
use App\Utils\Format;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Log;

class GenericController extends Controller
{
    const ENTIDAD = ["Libros","Isbn","Categorias","LibrosEstados","Idiomas","Editorial","Autores","MetodoDePago","Distribuidores","Facturas","Compras","Boletas","Ventas","Clientes","Trabajadores","Arriendos"];
    const IMPORT = 'App\\Models\\';
    protected $class;
    protected $error = null;
    protected $custom;
    /**
     * Create a new Class.
     *
     * @return void
     */
    public function __construct(Request $request, $custom = false)
    {
        $this->custom = $custom;
        if (in_array(ucfirst($request->entidad), ["LibrosEstados","MetodoDePago"])) {
            $this->class = self::IMPORT . ucfirst($request->entidad);
            return;
        }
        $this->class = self::IMPORT . Format::entidadClass($request->entidad);
        if (!$custom && !in_array(ucfirst(mb_strtolower($request->entidad, 'UTF-8')), self::ENTIDAD)) {
            $this->error = $this->responseFail(['message' => 'Entidad ingresada invalida']);
        }
    }

    /**
     * Display a listing of the entity.
     * @bodyParam entity string required token a verificar.
     * @authenticated
     */
    public function index(Request $request)
    {
        if ($this->error) {
            return $this->error;
        }
        $newClass = new $this->class();
        $with = [];

        if (method_exists($newClass, 'withTable') && $newClass->withTable() !== null) {
            $with = array_merge($with, $newClass->withTable());
        }
        if (method_exists($newClass, 'withTableMany')) {
            $with = array_merge($with, $newClass->withTableMany());
        }
        $perPage = 20;
        if ($request->has('perPage')) {
            $perPage = $request->perPage > 100 ? 100 : $request->perPage;
        }
        $item = $this->class::with($with)->orderBy('id', 'asc')->paginate($perPage);

        return $this->responseSuccess($item);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        if ($this->error) {
            return $this->error;
        }
        $newClass = new $this->class();
        $item = collect();
        if (method_exists($newClass, 'withTable') && $newClass->withTable() !== null) {
            $item = $this->class::with($newClass->withTable())->get();
            return $this->custom ? $item : $this->responseSuccess($item);
        }
        if (method_exists($newClass, 'mapColumns') && $newClass->mapColumns() !== null) {
            $item = $this->class::get($newClass::mapColumns());
            return $this->custom ? $item : $this->responseSuccess($item);
        }
        $item = $this->class::orderBy('id', 'asc')->get();
        return $this->custom ? $item : $this->responseSuccess($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        if ($this->error) {
            return $this->error;
        }
        try {
            $item = $this->class::where('id', $id)->firstOrFail();
            return $this->responseSuccess($item);
        } catch (QueryException $e) {
            return $this->responseFail(['message' => $e->getMessage()], 404);
        } catch (ModelNotFoundException $e) {
            return $this->responseFail(['message' => Format::entidadClass($request->entidad) . ' no encontrado'], 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->error) {
            return $this->error;
        }
        $this->validateFunction($request, $this->class::rules());
        $newRecord = new $this->class();
        $this->class::data($request, $newRecord);
        $newRecord->save();
        if (method_exists($newRecord, 'dataSync')) {
            $this->class::dataSync($request, $newRecord);
        }
        return $this->responseSuccess(['id' => $newRecord->id]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$entidad,$id)
    {
        if ($this->error) {
            return $this->error;
        }
        $this->validateFunction($request, $this->class::rules());
        try {
            $item = $this->class::where('id', $id)->firstOrFail();
            $this->class::data($request, $item);
            $item->save();
            if (method_exists($item, 'dataSync')) {
                $this->class::dataSync($request, $item);
            }
            return $this->custom ? $item : $this->responseSuccess(['id' => $item->id]);
        } catch (QueryException $e) {
            return $this->responseFail(['message' => $e->getMessage()], 404);
        } catch (ModelNotFoundException $e) {
            return $this->responseFail(['message' => Format::entidadClass($request->entidad) . ' no encontrado'], 404);
        }
    }


    public function form()
    {
        if ($this->error) {
            return $this->error;
        }
        $newClass = new $this->class();
        $withTable = [];
        $whereNotIn = method_exists($newClass, 'whereNotIn') ? $newClass->whereNotIn() : ['ID', 'CREATED_AT', 'UPDATED_AT', 'DELETED_AT', 'LOGIN_AT'];
        $items = UserTabCols::where('TABLE_SCHEMA' , 'biblioteca')->where('TABLE_NAME', $newClass->table())
            ->whereNotIn('COLUMN_NAME', $whereNotIn)
            ->orderBy('ORDINAL_POSITION', 'asc')
            ->get(['COLUMN_NAME as label' , 'DATA_TYPE as type_bd' ,'IS_NULLABLE as nullable']);

        if (method_exists($newClass, 'withTable')) {
            $withTable = $newClass->withTable();
        }
        $overwriteItems = method_exists($newClass, 'overwriteItems') ? $newClass->overwriteItems() : false;
        foreach ($items as $index => $item) {
            $item->name = strtolower($item->label);
            if ($overwriteItems && array_key_exists($item->name, $overwriteItems)) {
                $items[$index] = $overwriteItems[$item->name];
                continue;
            }
            $item->label = ucfirst($item->name);
            $item->inputRequired = $item->nullable === 'NO' ? true : false;
            $item->error = false;
            $item->disabled = false;
            $item->inputType = 'TextInput';


            if ($item->type_bd === "CHAR") {
                $item->inputType = 'Checkbox';
                $item->inputRequired = false;
            }

            if ($item->type_bd === "date") {
                $item->type = 'date';
            }
            if ($item->type_bd === "decimal" || $item->type_bd === "int") {
                $item->type = 'number';
            }

            if (method_exists($newClass, 'isDisabled')) {
                if (in_array(strtolower($item->label), $newClass->isDisabled())) $item->disabled = true;
            }

            if (method_exists($newClass, 'isRequired')) {
                if (in_array(strtolower($item->label), $newClass->isRequired())) $item->inputRequired = true;
            }
            if (method_exists($newClass, 'isNotRequired')) {
                if (in_array(strtolower($item->label), $newClass->isNotRequired())) $item->inputRequired = false;
            }
            if (method_exists($newClass, 'isValidate')) {
                if (array_key_exists(strtolower($item->label), $newClass->isValidate())) $item->validations = $newClass->isValidate()[strtolower($item->label)];
            }
            if ($item->type_bd === "bigint" && strpos($item->label, '_id')) {
                foreach ($withTable as $key => $table) {
                    if ($item->label === ucfirst($table) . '_id' || $this->evaluateWithoutUnderscore($item->label, $table)) {
                        $otherClass = method_exists($newClass, 'relationRename') ? self::IMPORT . $newClass->relationRename()[$table] : self::IMPORT . ucfirst($table);
                        $dataOtherClass = [];
                        if (method_exists($newClass, 'ormModel') && isset($newClass->ormModel()[$table]) && method_exists($otherClass, $newClass->ormModel()[$table])) {
                            $method = $newClass->ormModel()[$table];
                            $dataOtherClass = $otherClass::$method();
                        } else {
                            $otherClassWhere = method_exists($otherClass, 'formWhere') ? $otherClass::formWhere() : ['where' => ['activo', true]];
                            $dataOtherClass = $this->handleDataOtherClass($otherClassWhere, $otherClass, $newClass);
                        }
                        $item->items = $dataOtherClass;
                        $item->label = str_replace('_id', '', $item->label);
                        $item->inputType = 'Select';
                        $item->native = true;
                        $item->groupDeep = 1;
                        unset($withTable[$key]);
                        break;
                    }
                }
            }
            //$item->label = Format::changeLabel($item->name);
            $item->label = str_replace('_', ' ', ucwords($item->label, '_'));
            unset($item->nullable);
            unset($item->type_bd);
        }
        if (method_exists($newClass, 'withTableMany')) {
            foreach ($newClass->withTableMany() as $key => $table) {
                $otherClass = self::IMPORT . $table;
                $otherItem['name'] = Str::snake($table);
                $otherItem['label'] = $table;
                $otherItem['inputRequired'] = false;
                $otherItem['inputType'] = 'Select';
                $otherItem['mode'] = 'multiple';
                $otherItem['items'] = $otherClass::get($otherClass::mapColumns());
                $items->push($otherItem);
            }
        }
        return $this->responseSuccess($items);
    }


    public function destroy($entidad,$id)
    {
        if ($this->error) {
            return $this->error;
        }
        $item = $this->class::where('id', $id)->first();
        if ($item) {
            $item->activo = false;
            $item->delete();
            return $this->responseSuccess(['id' => $item->id]);
        }
        return $this->responseFail(['message' => 'No encontrado'], 412);
    }


    protected function handleDataOtherClass($otherClassWhere, $otherClass, $newClass)
    {
        $otherClass = new $otherClass();
        $mapColumns = $otherClass->mapColumns();
        if ($otherClassWhere['where']) {
            $otherClass = $otherClass->where([$otherClassWhere['where']]);
        }
        if (method_exists($newClass, 'formWhereNotIn')) {
            $conditions = $newClass->formWhereNotIn();
            $otherClass = $otherClass->whereNotIn($conditions[0], $conditions[1]);
        }
        return $otherClass->get($mapColumns);
    }

    protected function evaluateWithoutUnderscore($label, $table)
    {
        $label =  str_replace('_', '', $label);
        return strtolower($label) === strtolower(ucfirst($table)) . 'id';
    }



}
