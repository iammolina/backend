<?php

namespace App\Http\Controllers;
use App\Models\{Libros  , MetodoDePago , Arriendos , DetalleArriendos , Clientes , Trabajadores , Boletas , Ventas , DetalleVentas};
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ArriendoContoller extends Controller
{
    public function index(Request $request)
    {
        $form  = [];
        $formDetalle = [];
        $libros = Libros::get(['id as value' , 'titulo as label']);
        $clientes = Clientes::get(['id as value' , 'nombres as label']);
        $trabajadores = Trabajadores::get(['id as value' , 'nombres as label']);

        $form[0]['name'] = 'libro';
        $form[0]['label'] = 'Libro';
        $form[0]['inputRequired'] = true;
        $form[0]['inputType'] = 'Select';
        $form[0]['items'] = $libros;
        $form[1]['name'] = 'cantidad';
        $form[1]['label'] = 'Cantidad de libros';
        $form[1]['inputRequired'] = true;
        $form[1]['inputType'] = 'TextInput';
        $form[1]['inputType'] = 'TextInput';
        $form[1]['type'] = 'Number';
        $form[2]['name'] = 'valor';
        $form[2]['label'] = 'Valor del libros';
        $form[2]['inputRequired'] = true;
        $form[2]['inputType'] = 'TextInput';
        $form[2]['inputType'] = 'TextInput';
        $form[2]['type'] = 'Number';

        $formDetalle[0]['name'] = 'cliente';
        $formDetalle[0]['label'] = 'Clientes';
        $formDetalle[0]['inputRequired'] = true;
        $formDetalle[0]['inputType'] = 'Select';
        $formDetalle[0]['items'] = $clientes;

        $formDetalle[1]['name'] = 'trabajador';
        $formDetalle[1]['label'] = 'Trabajadores';
        $formDetalle[1]['inputRequired'] = true;
        $formDetalle[1]['inputType'] = 'Select';
        $formDetalle[1]['items'] = $trabajadores;

        $formDetalle[2]['name'] = 'fechaArriendo';
        $formDetalle[2]['label'] = 'Fecha Arriendo';
        $formDetalle[2]['inputRequired'] = true;
        $formDetalle[2]['inputType'] = 'TextInput';
        $formDetalle[2]['type'] = 'Date';

        $formDetalle[3]['name'] = 'fechaDevolucion';
        $formDetalle[3]['label'] = 'Fecha Devolucion';
        $formDetalle[3]['inputRequired'] = true;
        $formDetalle[3]['inputType'] = 'TextInput';
        $formDetalle[3]['type'] = 'Date';

        return $this->responseSuccess([ 'form' => $form , 'formDetalle' => $formDetalle , 'libros' => $libros  , 'clientes' => $clientes , 'trabajadores' => $trabajadores]);
    }

    public function formPay($id)
    {
        $formDetalle = [];
        $arriendo = Arriendos::where('id',$id)->first();
       
        if($arriendo){
           $fechaHoy =  Carbon::now();
           $fechaDevolucion =  Carbon::create($arriendo->fecha_de_devolucion_estimada);
           $dias = $fechaDevolucion->diffInDays($fechaHoy) > 0 ? $fechaDevolucion->diffInDays($fechaHoy) : 0;
           $multa = $dias > 0 ? $arriendo->costo_de_arriendo * $dias : 0;
           $costoTotal = intVal($multa) + $arriendo->costo_de_arriendo;
           $metodoDePago = MetodoDePago::get(['id as value' , 'nombre as label']);
            $formDetalle[0]['name'] = 'diasRetraso';
            $formDetalle[0]['label'] = 'Dias Retraso';
            $formDetalle[0]['inputType'] = 'TextInput';
            $formDetalle[0]['disabled'] = true;
            $formDetalle[1]['name'] = 'multa';
            $formDetalle[1]['label'] = 'Multa';
            $formDetalle[1]['inputType'] = 'TextInput';
            $formDetalle[1]['disabled'] = true;
            $formDetalle[2]['name'] = 'costoTotal';
            $formDetalle[2]['label'] = 'Costo Total';
            $formDetalle[2]['inputType'] = 'TextInput';
            $formDetalle[3]['name'] = 'metodoDePago';
            $formDetalle[3]['label'] = 'Metodo de Pago';
            $formDetalle[3]['inputRequired'] = true;
            $formDetalle[3]['inputType'] = 'Select';
            $formDetalle[3]['items'] = $metodoDePago;
        }
        return $this->responseSuccess(['data' => [ 'diasRetraso' => $dias , 'multa' => $multa , 'costoTotal' => $costoTotal  ] , 'form' => $formDetalle ]);
    }


    public function pay(Request $request){
        $arriendo = Arriendos::with(['Clientes:id,nombres','Trabajadores:id,nombres','DetalleArriendos'])->where('id', $request->id)->first();
        $arriendo->multa = $request->multa;
        $arriendo->costo_total = $request->costoTotal;
        $arriendo->dias_de_retraso = $request->diasRetraso;
        $arriendo->fecha_de_entrega_real = Carbon::now();
        $newBoleta = new Boletas();
        $request->merge(['items' => $arriendo['detallearriendos']]);
        $valores = $this->Valores($request , $request->multa / 1.19);
        $folio = $this->getFolio();
        $newBoleta->folio = $folio;
        $newBoleta->precio_neto = $valores['precio_neto'];
        $newBoleta->precio_con_iva = $valores['precio_con_iva'];
        $newBoleta->costo_iva = $valores['costo_iva'];
        $newBoleta->fecha_de_venta = Carbon::now();
        $newBoleta->clientes_id = $arriendo->clientes['id'];
        $newBoleta->trabajadores_id = $arriendo->trabajadores['id'];
        $newBoleta->metodo_de_pago_id = $request->metodoDePago;
        $newBoleta->save();
        $id = $newBoleta->id;
        $arriendo->boleta = $folio;
        $ventas = new Ventas();
        $ventas->boletas_id = $id;
        $ventas->fecha_creacion = Carbon::now();
        $ventas->save();
        $idVentas = $ventas->id;
        collect($arriendo['detallearriendos'])->each(function ($item, $key) use ($idVentas,$request){
              $detalleVentas = new DetalleVentas();
              $detalleVentas->ventas_id = $idVentas;
              $detalleVentas->libros_id  = $item->libros_id;
              $detalleVentas->cantidad  = intVal($item->cantidad);
              $detalleVentas->valor  = intVal($item->valor);
              $detalleVentas->save();
        });
        $arriendo->update();
        return $this->responseSuccess(['id'=>$arriendo->id]);
    }

    public function store(Request $request)
    {
        $newArriendo = new Arriendos();
        $valores = $this->Valores($request);
        $newArriendo->costo_total = 0;
        $newArriendo->multa = 0;
        $newArriendo->dias_de_retraso = 0;
        $newArriendo->boleta = 0;
        $newArriendo->costo_de_arriendo = $valores['precio_con_iva'];
        $newArriendo->fecha_de_arriendo = isset($request->detalle['fechaArriendo']) ? $request->detalle['fechaArriendo'] : 0;
        $newArriendo->fecha_de_devolucion_estimada = isset($request->detalle['fechaDevolucion']) ? $request->detalle['fechaDevolucion'] : 0;
        $newArriendo->clientes_id = isset($request->detalle['clienteID']) ? $request->detalle['clienteID'] : 0;
        $newArriendo->trabajadores_id = isset($request->detalle['trabajadorID']) ? $request->detalle['trabajadorID'] : 0;
        $newArriendo->save();
        $idArriendo = $newArriendo->id;
        collect($request->items)->each(function ($item, $key) use ($idArriendo,$request){
              $detalleArriendo = new DetalleArriendos();
              $detalleArriendo->arriendos_id = $idArriendo;
              $detalleArriendo->libros_id  = $item['idLibro'];
              $detalleArriendo->cantidad  = intVal($item['cantidad']);
              $detalleArriendo->valor  = intVal($item['valor']);
              $detalleArriendo->save();
        });
        return $this->responseSuccess(['id'=>$idArriendo]);
    }

    public static function Valores($request , $valor = 0){
        $precio_con_iva = 0;
        $costo_iva = 0;
        $newValores = collect($request->items)->sum('valor') + $valor;
        $iva = round($newValores * 1.19);
        return ['precio_neto' => $newValores , 'precio_con_iva' => $iva  , 'costo_iva' => round($iva - $newValores) ];
    }


    public static function getFolio()
    {
        $factura = Boletas::latest()->first(['folio']);
        if ($factura) {
            return ++$factura->folio;
        }
        return '1';
    }

    public function getID($id){
        $data = [];
        $factura = Arriendos::with(['Clientes:id,nombres' , 'Trabajadores:id,nombres','DetalleArriendos'])->where('id',$id)->first();
        if($factura){
         $data['detalle']['id'] = $factura->id;
         $data['detalle']['clienteName'] = $factura->clientes['nombres'];
         $data['detalle']['trabajadorName'] = $factura->trabajadores['nombres'];
         $data['detalle']['fechaArriendo'] = $factura->fecha_de_arriendo;
         $data['detalle']['fechaDevolucion'] = $factura->fecha_de_devolucion_estimada;
         $data['detalle']['CostoArriendo'] = $factura->costo_de_arriendo;
         $data['detalle']['fechaEntrega'] = $factura->fecha_de_entrega_real;
         $data['detalle']['Multa'] = $factura->multa;
         $data['detalle']['CostoTotal'] = $factura->costo_total;
         $data['detalle']['DiasRetraso'] = $factura->dias_de_retraso;
         $data['items'] = collect($factura['detallearriendos'])->map(function ($arriendo) {
                $item  = [];
                $item['cantidad'] = $arriendo->cantidad;
                $item['valor'] = $arriendo->valor;
                $item['libro'] = $arriendo->libros->titulo;
                return $item;
            });
        }
        return $this->responseSuccess($data);
    }
}
