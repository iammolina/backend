<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Libros , Distribuidores , MetodoDePago , Facturas , DetalleCompras , Compras};
use Illuminate\Support\Collection;



class FacturaController extends Controller
{
    public function index(Request $request)
    {
        $form  = [];
        $formDetalle = [];
        $libros = Libros::get(['id as value' , 'titulo as label']);
        $distribuidores = Distribuidores::get(['id as value' , 'nombres as label']);
        $metodoDePago = MetodoDePago::get(['id as value' , 'nombre as label']);

        $form[0]['name'] = 'libro';
        $form[0]['label'] = 'Libro';
        $form[0]['inputRequired'] = true;
        $form[0]['inputType'] = 'Select';
        $form[0]['items'] = $libros;
        $form[1]['name'] = 'cantidad';
        $form[1]['label'] = 'Cantidad de libros';
        $form[1]['inputRequired'] = true;
        $form[1]['inputType'] = 'TextInput';
        $form[1]['inputType'] = 'TextInput';
        $form[1]['type'] = 'Number';
        $form[2]['name'] = 'valor';
        $form[2]['label'] = 'Valor del libros';
        $form[2]['inputRequired'] = true;
        $form[2]['inputType'] = 'TextInput';
        $form[2]['inputType'] = 'TextInput';
        $form[2]['type'] = 'Number';

        $formDetalle[0]['name'] = 'distribuidor';
        $formDetalle[0]['label'] = 'Distribuidor';
        $formDetalle[0]['inputRequired'] = true;
        $formDetalle[0]['inputType'] = 'Select';
        $formDetalle[0]['items'] = $distribuidores;

        $formDetalle[1]['name'] = 'metodoDePago';
        $formDetalle[1]['label'] = 'Metodo de Pago';
        $formDetalle[1]['inputRequired'] = true;
        $formDetalle[1]['inputType'] = 'Select';
        $formDetalle[1]['items'] = $metodoDePago;

        $formDetalle[2]['name'] = 'fechaFactura';
        $formDetalle[2]['label'] = 'Fecha Factura';
        $formDetalle[2]['inputRequired'] = true;
        $formDetalle[2]['inputType'] = 'TextInput';
        $formDetalle[2]['type'] = 'Date';

        return $this->responseSuccess([ 'form' => $form , 'formDetalle' => $formDetalle , 'libros' => $libros , 'metodosDePago' => $metodoDePago , 'distribuidores' => $distribuidores]);
    }

    public function store(Request $request)
    {
        $newFactura = new Facturas();
        $valores = $this->Valores($request);
        $newFactura->folio = $request->has('folio') ? $request->folio : $this->getFolio();
        $newFactura->precio_neto = $valores['precio_neto'];
        $newFactura->precio_con_iva = $valores['precio_con_iva'];
        $newFactura->costo_iva = $valores['costo_iva'];
        $newFactura->fecha_de_compra = isset($request->detalle['fechaFactura']) ? $request->detalle['fechaFactura'] : 0;
        $newFactura->distribuidores_id = isset($request->detalle['distribuidorId']) ? $request->detalle['distribuidorId'] : 0;
        $newFactura->metodo_de_pago_id = isset($request->detalle['metodoPagoId']) ? $request->detalle['metodoPagoId'] : 0;
        $newFactura->save();
        $id = $newFactura->id;
        $compras = new Compras();
        $compras->facturas_id = $id;
        $compras->fecha_creacion = isset($request->detalle['fechaFactura']) ? $request->detalle['fechaFactura'] : 0;
        $compras->save();
        $idCompras = $compras->id;
        collect($request->items)->each(function ($item, $key) use ($idCompras,$request){
              $detalleCompras = new DetalleCompras();
              $detalleCompras->compras_id = $idCompras;
              $detalleCompras->libros_id  = $item['idLibro'];
              $detalleCompras->distribuidores_id  = isset($request->detalle['distribuidorId']) ? $request->detalle['distribuidorId'] : 0;
              $detalleCompras->cantidad  = intVal($item['cantidad']);
              $detalleCompras->valor  = intVal($item['valor']);
              $detalleCompras->save();
        });
        return $this->responseSuccess(['id'=>$idCompras]);
    }

    public static function Valores($request){
        $precio_con_iva = 0;
        $costo_iva = 0;
        $newValores = collect($request->items)->sum('valor');
        $iva = round($newValores * 1.19);
        return ['precio_neto' => $newValores , 'precio_con_iva' => $iva  , 'costo_iva' => round($iva - $newValores) ];
    }


    public static function getFolio()
    {
        $factura = Facturas::latest()->first(['folio']);
        if ($factura) {
            return ++$factura->folio;
        }
        return '1';
    }

    public function getID($id){
        $factura = Facturas::with(['distribuidores:id,nombres','metododepago:id,nombre','compras.detallecompras.libros:id,titulo'])->where('folio',$id)->first();
        if($factura){
         $data['detalle']['folio'] = $factura->folio;
         $data['detalle']['distribuidorName'] = $factura->distribuidores['nombres'];
         $data['detalle']['metodoName'] = $factura->metododepago['nombre'];
         $data['detalle']['fechaFactura'] = $factura->fecha_de_compra;
         $data['items'] = collect($factura['compras']['detallecompras'])->map(function ($data) {
                $item['cantidad'] = $data->cantidad;
                $item['valor'] = $data->valor;
                $item['libro'] = $data->libros->titulo;
                return $item;
            });
        }

        return $this->responseSuccess($data);
    }

    

}
